<?php

namespace App\Form;

use App\Form\SdSportType;
use App\Entity\SdPratique;
use App\Form\SdPersonneType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class SdPratiqueType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id_personne', SdPersonneType::class)
            ->add('id_sport', SdSportType::class)
            ->add('niveau', ChoiceType::class, [
                'choices' => [
                    'Débutant' => 'Débutant',
                    'Confirmé' => 'Confirmé',
                    'Pro' => 'Pro',
                    'Supporter' => 'Supporter'
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SdPratique::class,
        ]);
    }
}
