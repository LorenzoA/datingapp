<?php

namespace App\Controller;

use App\Entity\SdPersonne;
use App\Form\SdPersonneType;
use App\Repository\SdPersonneRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/sd/personne")
 */
class SdPersonneController extends AbstractController
{
    /**
     * @Route("/", name="sd_personne_index", methods={"GET"})
     */
    public function index(SdPersonneRepository $sdPersonneRepository): Response
    {
        return $this->render('sd_personne/index.html.twig', [
            'sd_personnes' => $sdPersonneRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="sd_personne_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $sdPersonne = new SdPersonne();
        $form = $this->createForm(SdPersonneType::class, $sdPersonne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($sdPersonne);
            $entityManager->flush();

            return $this->redirectToRoute('sd_personne_index');
        }

        return $this->render('sd_personne/new.html.twig', [
            'sd_personne' => $sdPersonne,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sd_personne_show", methods={"GET"})
     */
    public function show(SdPersonne $sdPersonne): Response
    {
        return $this->render('sd_personne/show.html.twig', [
            'sd_personne' => $sdPersonne,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="sd_personne_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SdPersonne $sdPersonne): Response
    {
        $form = $this->createForm(SdPersonneType::class, $sdPersonne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sd_personne_index');
        }

        return $this->render('sd_personne/edit.html.twig', [
            'sd_personne' => $sdPersonne,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sd_personne_delete", methods={"DELETE"})
     */
    public function delete(Request $request, SdPersonne $sdPersonne): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sdPersonne->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($sdPersonne);
            $entityManager->flush();
        }

        return $this->redirectToRoute('sd_personne_index');
    }
}
