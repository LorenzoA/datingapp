<?php

namespace App\Controller;

use App\Entity\SdSport;
use App\Form\SdSportType;
use App\Repository\SdSportRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/sd/sport")
 */
class SdSportController extends AbstractController
{
    /**
     * @Route("/", name="sd_sport_index", methods={"GET"})
     */
    public function index(SdSportRepository $sdSportRepository): Response
    {
        return $this->render('sd_sport/index.html.twig', [
            'sd_sports' => $sdSportRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="sd_sport_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $sdSport = new SdSport();
        $form = $this->createForm(SdSportType::class, $sdSport);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($sdSport);
            $entityManager->flush();

            return $this->redirectToRoute('sd_sport_index');
        }

        return $this->render('sd_sport/new.html.twig', [
            'sd_sport' => $sdSport,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sd_sport_show", methods={"GET"})
     */
    public function show(SdSport $sdSport): Response
    {
        return $this->render('sd_sport/show.html.twig', [
            'sd_sport' => $sdSport,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="sd_sport_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SdSport $sdSport): Response
    {
        $form = $this->createForm(SdSportType::class, $sdSport);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sd_sport_index');
        }

        return $this->render('sd_sport/edit.html.twig', [
            'sd_sport' => $sdSport,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sd_sport_delete", methods={"DELETE"})
     */
    public function delete(Request $request, SdSport $sdSport): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sdSport->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($sdSport);
            $entityManager->flush();
        }

        return $this->redirectToRoute('sd_sport_index');
    }
}
