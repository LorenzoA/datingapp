<?php

namespace App\Controller;

use App\Entity\SdSport;
use App\Entity\SdPersonne;
use App\Entity\SdPratique;
use App\Form\SdPratiqueType;
use App\Repository\SdPratiqueRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/")
 */
class SdPratiqueController extends AbstractController
{
    /**
     * @Route("/", name="sd_pratique_index", methods={"GET"})
     */
    public function index(SdPratiqueRepository $sdPratiqueRepository): Response
    {
        return $this->render('sd_pratique/index.html.twig', [
            'sd_pratiques' => $sdPratiqueRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="sd_pratique_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $sdPratique = new SdPratique();
        $form = $this->createForm(SdPratiqueType::class, $sdPratique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $sportAjout = $entityManager->getRepository(SdSport::class);
            $sdPratique->getIdSport()->setNomSport($sportAjout->find(1));
            // $mailAjout = $entityManager->getRepository(SdPersonne::class);
            // $sdPratique->getMail()->setMail($mailAjout->find(1));
            $entityManager->persist($sdPratique);
            $entityManager->flush();

            return $this->redirectToRoute('sd_pratique_index');
        }

        return $this->render('sd_pratique/new.html.twig', [
            'sd_pratique' => $sdPratique,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sd_pratique_show", methods={"GET"})
     */
    public function show(SdPratique $sdPratique): Response
    {
        return $this->render('sd_pratique/show.html.twig', [
            'sd_pratique' => $sdPratique,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="sd_pratique_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SdPratique $sdPratique): Response
    {
        $form = $this->createForm(SdPratiqueType::class, $sdPratique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sd_pratique_index');
        }

        return $this->render('sd_pratique/edit.html.twig', [
            'sd_pratique' => $sdPratique,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sd_pratique_delete", methods={"DELETE"})
     */
    public function delete(Request $request, SdPratique $sdPratique): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sdPratique->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($sdPratique);
            $entityManager->flush();
        }

        return $this->redirectToRoute('sd_pratique_index');
    }
}
