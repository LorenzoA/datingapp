<?php

namespace App\Repository;

use App\Entity\SdPratique;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SdPratique|null find($id, $lockMode = null, $lockVersion = null)
 * @method SdPratique|null findOneBy(array $criteria, array $orderBy = null)
 * @method SdPratique[]    findAll()
 * @method SdPratique[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SdPratiqueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SdPratique::class);
    }

    // /**
    //  * @return SdPratique[] Returns an array of SdPratique objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SdPratique
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
