<?php

namespace App\Repository;

use App\Entity\SdSport;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SdSport|null find($id, $lockMode = null, $lockVersion = null)
 * @method SdSport|null findOneBy(array $criteria, array $orderBy = null)
 * @method SdSport[]    findAll()
 * @method SdSport[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SdSportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SdSport::class);
    }

    // /**
    //  * @return SdSport[] Returns an array of SdSport objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SdSport
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
