<?php

namespace App\Entity;

use App\Entity\SdPratique;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\SdSportRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=SdSportRepository::class)
 * @UniqueEntity(fields={"nom_sport"}, message="Il semble que ce sport est déjà dans la liste !")
 */
class SdSport
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="nom_sport", type="string", length=320, unique=true)
     */
    private $nom_sport;

    public function __toString(){
        return $this->nom_sport;
    }

    /**
     * @ORM\OneToMany(targetEntity=SdPratique::class, mappedBy="id_sport")
     */
    private $id_sport;

    public function __construct()
    {
        $this->id_sport = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomSport(): ?string
    {
        return $this->nom_sport;
    }

    public function setNomSport(string $nom_sport): self
    {
        $this->nom_sport = $nom_sport;

        return $this;
    }

    /**
     * @return Collection|SdPratique[]
     */
    public function getIdSport(): Collection
    {
        return $this->id_sport;
    }

    public function addIdSport(SdPratique $idSport): self
    {
        if (!$this->id_sport->contains($idSport)) {
            $this->id_sport[] = $idSport;
            $idSport->setIdSport($this);
        }

        return $this;
    }

    public function removeIdSport(SdPratique $idSport): self
    {
        if ($this->id_sport->removeElement($idSport)) {
            // set the owning side to null (unless already changed)
            if ($idSport->getIdSport() === $this) {
                $idSport->setIdSport(null);
            }
        }

        return $this;
    }
}
