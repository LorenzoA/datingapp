<?php

namespace App\Entity;

use App\Entity\SdSport;
use App\Entity\SdPersonne;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\SdPratiqueRepository;

/**
 * @ORM\Entity(repositoryClass=SdPratiqueRepository::class)
 */
class SdPratique
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=SdPersonne::class, inversedBy="id_personne", cascade={"persist", "remove", "refresh"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_personne;

    /**
     * @ORM\ManyToOne(targetEntity=SdSport::class, inversedBy="id_sport", cascade={"persist", "remove", "refresh"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_sport;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $niveau;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdPersonne(): ?SdPersonne
    {
        return $this->id_personne;
    }

    public function setIdPersonne(?SdPersonne $id_personne): self
    {
        $this->id_personne = $id_personne;

        return $this;
    }

    public function getIdSport(): ?SdSport
    {
        return $this->id_sport;
    }

    public function setIdSport(?SdSport $id_sport): self
    {
        $this->id_sport = $id_sport;

        return $this;
    }

    public function getNiveau(): ?string
    {
        return $this->niveau;
    }

    public function setNiveau(string $niveau): self
    {
        $this->niveau = $niveau;

        return $this;
    }
}
