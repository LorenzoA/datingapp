<?php

namespace App\Entity;

use App\Entity\SdPratique;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\SdPersonneRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=SdPersonneRepository::class)
 * @UniqueEntity(fields={"mail"}, message="Il semble que vous ayez déjà ajouté l'email {mail}!")
 */
class SdPersonne
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $departement;

    /**
     * @ORM\Column(name="mail", type="string", length=320, unique=true)
     * @Assert\Email
     */
    private $mail;

    /**
     * @ORM\OneToMany(targetEntity=SdPratique::class, mappedBy="id_personne")
     */
    private $id_personne;

    public function __construct()
    {
        $this->id_personne = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getDepartement(): ?string
    {
        return $this->departement;
    }

    public function setDepartement(string $departement): self
    {
        $this->departement = $departement;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * @return Collection|SdPratique[]
     */
    public function getIdSport(): Collection
    {
        return $this->id_sport;
    }

    public function addIdSport(SdPratique $idSport): self
    {
        if (!$this->id_sport->contains($idSport)) {
            $this->id_sport[] = $idSport;
            $idSport->setIdPersonne($this);
        }

        return $this;
    }

    public function removeIdSport(SdPratique $idSport): self
    {
        if ($this->id_sport->removeElement($idSport)) {
            // set the owning side to null (unless already changed)
            if ($idSport->getIdPersonne() === $this) {
                $idSport->setIdPersonne(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SdPratique[]
     */
    public function getIdPersonne(): Collection
    {
        return $this->id_personne;
    }

    public function addIdPersonne(SdPratique $idPersonne): self
    {
        if (!$this->id_personne->contains($idPersonne)) {
            $this->id_personne[] = $idPersonne;
            $idPersonne->setIdPersonne($this);
        }

        return $this;
    }

    public function removeIdPersonne(SdPratique $idPersonne): self
    {
        if ($this->id_personne->removeElement($idPersonne)) {
            // set the owning side to null (unless already changed)
            if ($idPersonne->getIdPersonne() === $this) {
                $idPersonne->setIdPersonne(null);
            }
        }

        return $this;
    }
}
